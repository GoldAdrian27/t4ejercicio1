
public class Ejercicio1App {

	public static void main(String[] args) {

		int num1 = 5;
		int num2 = 9;
		
		System.out.println("Suma " + (num1+num2));
		System.out.println("Resta " + (num1-num2));
		System.out.println("Multiplicación " + (num1*num2));
		System.out.println("División " + (num1/num2));
		System.out.println("Módulo " + (num1%num2));
	}

}
